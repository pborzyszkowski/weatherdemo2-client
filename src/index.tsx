import 'rxjs';

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {Provider} from 'react-redux'
import {applyMiddleware, createStore} from 'redux';
import {weatherApp} from './store/reducers/index';
import {AppContainer} from './app/app-container';
import {combineEpics, createEpicMiddleware} from "redux-observable";
import {weatherEffects} from "./store/effects/weather";

const epicMiddleware = createEpicMiddleware(
    (...args : any[]) => combineEpics(
        weatherEffects as any,
    )(...args, {fetch})
);

let store = createStore(
    weatherApp,
    (window as any).__REDUX_DEVTOOLS_EXTENSION__ && (window as any).__REDUX_DEVTOOLS_EXTENSION__(),
    applyMiddleware(epicMiddleware),
)

ReactDOM.render(
    <Provider store={store}>
        <AppContainer />
    </Provider>,
    document.getElementById('root') as HTMLElement
);
