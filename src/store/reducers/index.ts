import {combineReducers} from 'redux';
import {weatherReducer, WeatherState} from './weather';

export interface WeatherAppState {
    weather: WeatherState;
}

export const weatherApp = combineReducers({
    weather: weatherReducer,
});
