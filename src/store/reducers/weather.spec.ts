import {weatherReducer, WeatherState} from './weather';
import * as actions from '../actions/weather';
import {Location} from '../../model/location';
import {Weather} from '../../model/weather';

describe('reducers', () => {
    const location = {
        country: 'Poland',
        city: 'Gdańsk'
    } as Location;
    const weather = {
        humidity: 100,
        temperature: 50,
    } as Weather;

    it('should return the initial state', () => {
        expect(weatherReducer(undefined, {} as actions.WeatherAction)).toEqual({
            location: undefined,
            weather: undefined,
            isLoaded: false,
            isLoading: false,
        } as WeatherState);
    });

    it('should handle a receive action', () => {
        const action = {
            type: actions.WEATHER_RECEIVE_TYPE,
            location
        } as actions.ReceiveWeatherAction;

        expect(weatherReducer(undefined, action)).toEqual({
            location,
            weather: undefined,
            isLoaded: false,
            isLoading: true,
        } as WeatherState);
    });

    it('should handle a received action', () => {
        const action = {
            type: actions.WEATHER_RECEIVED_TYPE,
            weather
        } as actions.ReceivedWeatherAction;
        const state = {
            location,
            weather: undefined,
            isLoading: true,
            isLoaded: false,
        } as WeatherState;

        expect(weatherReducer(state, action)).toEqual({
            location,
            weather,
            isLoaded: true,
            isLoading: false,
        } as WeatherState);
    });

    it('should handle a reset action', () => {
        const action = {
            type: actions.RESET_TYPE,
        } as actions.ReceivedWeatherAction;
        const state = {
            location,
            weather,
            isLoaded: true,
            isLoading: false,
        } as WeatherState;

        expect(weatherReducer(state, action)).toEqual({
            location: undefined,
            weather: undefined,
            isLoaded: false,
            isLoading: false,
        } as WeatherState);
    });
});
