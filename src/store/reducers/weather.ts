import {Location} from '../../model/location';
import {Weather} from '../../model/weather';
import {
    ReceivedWeatherAction,
    ReceiveWeatherAction,
    ResetAction,
    WeatherAction,
    WEATHER_RECEIVE_TYPE,
    WEATHER_RECEIVED_TYPE,
    RESET_TYPE,
} from '../actions/weather';

export interface WeatherState {
    location?: Location;
    weather?: Weather;
    isLoaded: boolean;
    isLoading: boolean;
};

export const initialWeatherState: WeatherState = {
    location: undefined,
    weather: undefined,
    isLoaded: false,
    isLoading: false,
};

const actions = {};

actions[WEATHER_RECEIVE_TYPE] = (state: WeatherState, action: ReceiveWeatherAction) => {
    return Object.assign({}, state, {
        location: Object.assign({}, action.location),
        weather: undefined,
        isLoaded: false,
        isLoading: true,
    } as WeatherState);
};

actions[WEATHER_RECEIVED_TYPE] = (state: WeatherState, action: ReceivedWeatherAction) => {
    return Object.assign({}, state, {
        weather: action.weather,
        isLoaded: true,
        isLoading: false,
    } as WeatherState);
};

actions[RESET_TYPE] = (state: WeatherState, action: ResetAction) => {
    return initialWeatherState;
};

export const weatherReducer = (state: WeatherState = initialWeatherState, action: WeatherAction) => {
    const actionHandler = actions[action.type];
    return actionHandler ? actionHandler(state, action) : state;
};
