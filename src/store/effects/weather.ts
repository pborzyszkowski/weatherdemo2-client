import {WEATHER_RECEIVE_TYPE, ReceiveWeatherAction, received} from '../actions/weather';
import {WeatherResponse} from '../../model/weather-response';
import {Weather} from '../../model/weather';
import {ActionsObservable} from 'redux-observable';

export const weatherEffects =
    (action$: ActionsObservable<ReceiveWeatherAction>, store: any, {fetch}: GlobalFetch) => action$
        .ofType(WEATHER_RECEIVE_TYPE)
        .mergeMap(
            (action: ReceiveWeatherAction) => fetch(
                `/api/weather/${action.location.country}/${action.location.city}`
            )
                .then((response: Body) => response.json())
                .then((response: WeatherResponse) => received({
                    temperature: response.temperature.value,
                    humidity: response.humidity
                } as Weather))
                .catch((reason) => {
                    throw new Error(reason)
                })
        );
