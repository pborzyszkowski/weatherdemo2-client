import 'rxjs';

import * as actions from '../actions/weather';
import {WeatherAction} from '../actions/weather';
import {Location} from '../../model/location';
import {ActionsObservable} from 'redux-observable';
import {Weather} from '../../model/weather';
import {weatherEffects} from './weather';
import * as fetchMock from 'fetch-mock';
import {WeatherResponse} from '../../model/weather-response';
import {Temperature} from '../../model/temperature';
import {TemperatureUnit} from '../../model/temperature-unit';

describe('Weather async effects', () => {
    const location = {
        country: 'Poland',
        city: 'Gdansk'
    } as Location;
    const weather = {
        humidity: 100,
        temperature: 50,
    } as Weather;

    it('dispatches the correct actions when it is successful', (done) => {
        const action$ = ActionsObservable.of(
            {
                type: actions.WEATHER_RECEIVE_TYPE,
                location
            });
        const jsonResponse = JSON.stringify({
            location,
            humidity: weather.humidity,
            temperature: {
                value: weather.temperature,
                format: TemperatureUnit.Celsius
            } as Temperature
        } as WeatherResponse);

        const expectedOutputActions = [{
            type: actions.WEATHER_RECEIVED_TYPE,
            weather
        }] as WeatherAction[];

        // casting to any - ugly workaround for incomplete typing :(
        const myFetchMock = (fetchMock as any).sandbox()
            .mock('/api/weather/Poland/Gdansk', jsonResponse);

        weatherEffects(action$, null, {fetch: myFetchMock} as GlobalFetch)
            .toArray()
            .subscribe((actualOutputActions: WeatherAction[]) => {
                try {
                    expect(actualOutputActions).toEqual(expectedOutputActions);
                    done();
                } catch (ex) {
                    done.fail(ex);
                }
            });
    });
});
