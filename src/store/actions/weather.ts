import {Action} from 'redux';
import {Weather} from '../../model/weather';
import {Location} from '../../model/location';

export const WEATHER_RECEIVE_TYPE = 'WEATHER_RECEIVE_TYPE';
export const WEATHER_RECEIVED_TYPE = 'WEATHER_RECEIVED_TYPE';
export const RESET_TYPE = 'RESET_TYPE';

export interface ReceiveWeatherAction extends Action {
    location: Location
}

export interface ReceivedWeatherAction extends Action {
    weather: Weather;
}

export interface ResetAction extends Action {
}

export const receive = (location: Location): ReceiveWeatherAction => {
    return {
        type: WEATHER_RECEIVE_TYPE,
        location
    } as ReceiveWeatherAction;
};

export const received = (weather: Weather): ReceivedWeatherAction => {
    return {
        type: WEATHER_RECEIVED_TYPE,
        weather
    } as ReceivedWeatherAction;
};

export const reset = (): ResetAction => {
    return {
        type: RESET_TYPE
    } as ResetAction;
};

export type WeatherAction = ReceiveWeatherAction | ReceivedWeatherAction | ResetAction;
