import {Location} from '../../model/location';
import * as actions from '../actions/weather';
import {Weather} from '../../model/weather';

describe('actions', () => {
    it('should create an action to receive weather forecast', () => {
        const location = {
            country: 'Poland',
            city: 'Gdańsk'
        } as Location;
        const expectedAction = {
            type: actions.WEATHER_RECEIVE_TYPE,
            location
        } as actions.ReceiveWeatherAction;

        expect(actions.receive(location)).toEqual(expectedAction)
    });

    it('should create an action to notify that weather forecast has been received', () => {
        const weather = {
            temperature: 66,
            humidity: 100,
        } as Weather;
        const expectedAction = {
            type: actions.WEATHER_RECEIVED_TYPE,
            weather,
        } as actions.ReceivedWeatherAction;

        expect(actions.received(weather)).toEqual(expectedAction)
    });

    it('should create an action to reset state', () => {
        const expectedAction = {
            type: actions.RESET_TYPE,
        } as actions.ResetAction;

        expect(actions.reset()).toEqual(expectedAction)
    });
});
