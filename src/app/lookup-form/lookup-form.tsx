import * as React from 'react';
import {LookupFormProps, LookupFormPropsData} from './lookup-form-types';
import {Divider, Paper, RaisedButton, TextField} from 'material-ui';
import {FormEvent} from 'react';
import './lookup-form.css';

export class LookupForm extends React.Component<LookupFormProps, LookupFormPropsData> {
    constructor(props: LookupFormProps) {
        super(props);
        this.state = {
            city: props.city || '',
            country: props.country || '',
        }
    }

    private internalHandleSubmit(event: FormEvent<HTMLFormElement>): void {
        event.preventDefault();

        if (this.state.country && this.state.city) {
            this.props.handleSubmit({
                country: this.state.country,
                city: this.state.city,
            });
        }
    };

    public render(): any {
        return (
            <form onSubmit={(e) => this.internalHandleSubmit(e)}>
                <Paper>
                    <TextField
                        floatingLabelText="Country"
                        name="country"
                        value={this.state.country}
                        onChange={(e, value) => this.setState({country: value as string})}
                        underlineShow={false}
                        className="field"
                    />
                    <Divider/>
                    <TextField
                        floatingLabelText="City"
                        name="city"
                        value={this.state.city}
                        onChange={(e, value) => this.setState({city: value as string})}
                        underlineShow={false}
                        className="field"
                    />
                </Paper>
                <br/>
                <RaisedButton type="submit" label="Check" primary={true} className="checkButton"/>
            </form>
        );
    }
}
