import {LookupForm} from './lookup-form';
import {WeatherAppState} from '../../store/reducers/index';
import {connect} from 'react-redux';
import {LookupFormPropsData} from './lookup-form-types';
import {Location} from '../../model/location';
import {receive} from '../../store/actions/weather';

const mapStateToProps = (state: WeatherAppState): LookupFormPropsData => {
    return {
        country: state.weather.location ? state.weather.location.country : undefined,
        city: state.weather.location ? state.weather.location.city : undefined,
    };
};

const mapDispatchToProps = (dispatch: any) => {
    return {
        handleSubmit: (location: Location) => dispatch(receive(location)),
    }
}

export const LookupFormContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(LookupForm as any);
