import {Location} from '../../model/location';

export interface LookupFormProps extends LookupFormPropsData, LookupFormPropsDispatch {
}

export interface LookupFormPropsData {
    country?: string;
    city?: string;
}

export interface LookupFormPropsDispatch {
    handleSubmit(formData: Location): any;
}
