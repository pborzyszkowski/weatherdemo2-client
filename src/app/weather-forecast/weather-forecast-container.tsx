import {connect} from 'react-redux';
import {WeatherAppState} from '../../store/reducers/index';
import {WeatherForecastPropsData, WeatherForecastPropsDispatch} from './weather-forecast-types';
import {Weather} from '../../model/weather';
import {WeatherForecast} from './weather-forecast';
import {reset} from '../../store/actions/weather';

const mapStateToProps = (state: WeatherAppState): WeatherForecastPropsData => {
    return {
        weather: state.weather.weather as Weather
    };
};

const mapDispatchToProps = (dispatch: any): WeatherForecastPropsDispatch => {
    return {
        closeHandler: () => dispatch(reset()),
    }
}

export const WeatherForecastContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(WeatherForecast);
