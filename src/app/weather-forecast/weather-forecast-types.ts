import {Weather} from "../../model/weather";

export interface WeatherForecastProps extends WeatherForecastPropsData, WeatherForecastPropsDispatch {
}

export interface WeatherForecastPropsData {
    weather: Weather;
}

export interface WeatherForecastPropsDispatch {
    closeHandler(): void;
}

