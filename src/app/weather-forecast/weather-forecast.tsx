import * as React from 'react';
import {Paper, RaisedButton} from 'material-ui';
import 'font-awesome/css/font-awesome.css'
import {WeatherForecastProps} from './weather-forecast-types';
import './weather-forecast.css';

export function WeatherForecast(props: WeatherForecastProps) {
    return (
        <div>
            <Paper>
                <div className="valueContainer">
                    <div className="iconContainer">
                        <i className="fa fa-thermometer-half"/>
                    </div>
                    <div className="value temperatureValue">
                        {props.weather.temperature} &deg;C
                    </div>
                </div>
                <div className="valueContainer">
                    <div className="iconContainer">
                        <i className="fa fa-tint"/>
                    </div>
                    <div className="value humidityValue">
                        {props.weather.humidity} %
                    </div>
                </div>
            </Paper>
            <br/>
            <RaisedButton primary={true} label="Close" onClick={props.closeHandler} className="closeButton"/>
        </div>
    );
}