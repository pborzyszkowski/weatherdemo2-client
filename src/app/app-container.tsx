import {WeatherAppState} from '../store/reducers/index';
import {connect} from 'react-redux';
import {App, AppProps} from './app';

const mapStateToProps = (state: WeatherAppState): AppProps => {
    return {
        location: state.weather.location,
        isLoaded: state.weather.isLoaded,
        isLoading: state.weather.isLoading,
    };
};

export const AppContainer = connect(mapStateToProps)(App);
