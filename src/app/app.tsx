import * as React from 'react';
import {Card, CardHeader, CardText} from 'material-ui/Card';
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {LookupFormContainer} from './lookup-form/lookup-form-container';
import {WeatherForecastContainer} from './weather-forecast/weather-forecast-container';
import {Location} from '../model/location';
import {CircularProgress} from 'material-ui';

export interface AppProps {
    location?: Location;
    isLoaded: boolean;
    isLoading: boolean
}

export function App(props: AppProps) {
    return (
        <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
            {
                props.isLoading
                    ?
                    <CircularProgress/>
                    :
                    <Card>
                        <CardHeader title="Weather forecast"
                                    subtitle={props.location ? `${props.location.city}, ${props.location.country}` : undefined}/>
                        <CardText>
                            {
                                props.isLoaded
                                    ?
                                    <WeatherForecastContainer/>
                                    :
                                    <LookupFormContainer/>
                            }
                        </CardText>
                    </Card>
            }
        </MuiThemeProvider>
    );
}
