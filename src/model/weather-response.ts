import {Location} from "./location";
import {Temperature} from "./temperature";

export interface WeatherResponse {
    location: Location,
    temperature: Temperature,
    humidity: number
}
