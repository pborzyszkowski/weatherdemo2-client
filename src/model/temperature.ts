import {TemperatureUnit} from "./temperature-unit";

export interface Temperature {
    value: number,
    format: TemperatureUnit
}
