declare module 'material-ui/Card';
declare module 'material-ui/styles/MuiThemeProvider';
declare module 'material-ui/styles/getMuiTheme';
declare module 'material-ui/styles/baseThemes/lightBaseTheme';
declare module 'material-ui/styles/baseThemes/darkBaseTheme';
