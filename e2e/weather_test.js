Feature('Weather');

Scenario('Weather forecast can be checked and page flow works as expected', (I) => {
    I.amOnPage('/');
    I.dontSeeElement('valueContainer');
    I.fillField('input[name=country]', 'Poland');
    I.fillField('input[name=city]', 'Gdansk');
    I.click('.checkButton button');

    I.waitForElement('.valueContainer');
    I.dontSeeElement('input[name=country]');
    I.seeElement('.value.temperatureValue');
    I.seeElement('.value.humidityValue');
    I.click('.closeButton button');

    I.seeElement('input[name=country]');
    I.dontSeeElement('valueContainer');
});
